import styles from './Nav.scss'

export default function Nav() {
    return (
        <nav className={styles.nav}>
            <ul className={`${styles['list-reset']} ${styles.nav__items}`}>
                <a className={`${styles.nav__link}`} href="#"><li className={`${styles.nav__item}`}>Главная</li></a>
                <a className={`${styles.nav__link}`} href="#"><li className={`${styles.nav__item}`}>Пойманные покемоны</li></a>
            </ul>
        </nav>
    )
}