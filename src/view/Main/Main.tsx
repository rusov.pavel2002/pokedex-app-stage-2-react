import styles from './Main.scss'

import PokemonList from '../PokemonList/PokemonList'

export default function Main() {
    return (
        <main className={styles.main}>
            <PokemonList />
        </main>
    )
}