import { PokemonService } from "@/service/service"
import { PokemonData } from "@/model/model";
import PokemonCard from "../PokemonCard/PokemonCard";

import styles from './PokemonList.scss'
import { useEffect, useState, useCallback } from "react";

export default function PokemonList() {

    const pokemonServiceInstance = PokemonService.getInstance();
    const [pokemons, setPokemons] = useState<PokemonData[]>([]);
    const [loading, setLoading] = useState(false);

    const loadPokemons = useCallback(async () => {
        setLoading(true);
        try {
            const newPokemons = await pokemonServiceInstance.getDataFromApi();
            setPokemons(prevPokemons => [...prevPokemons, ...newPokemons]);
        } catch (err) {
            console.error(err);
        } finally {
            setLoading(false);
        }
    }, []);

    useEffect(() => {
        loadPokemons();
    }, [loadPokemons]);

    const handleScroll = useCallback(() => {
        if (document.documentElement.scrollHeight < document.documentElement.scrollTop + window.innerHeight + 100 && !loading) {
            loadPokemons();
        }
    }, [loading, loadPokemons])

    useEffect(() => {
        document.addEventListener("scroll", handleScroll);
        return () => {
            document.removeEventListener("scroll", handleScroll);
        }
    }, [handleScroll])

    return (
        <section id='pokemon-list' className={styles['pokemon-list']}>
            <div className={`${styles.container} ${styles['pokemon-list__container']}`}>
                <h2 className={styles.title}>Поймай своих покемонов!</h2>
                <div className={styles['pokemon-list__items']}>
                    {pokemons.map((pokemon, index) => (
                        <div key={index} className={styles['pokemon-card']}>
                            <PokemonCard name={pokemon.name} id={pokemon.id} avatar={pokemon.avatar} />
                        </div>
                    ))}
                </div>
            </div>
        </section>
    )
}