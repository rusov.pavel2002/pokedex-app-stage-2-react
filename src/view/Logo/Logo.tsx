import styles from './Logo.scss'

export default function Logo() {
    return (
        <a href="#" className={styles.logo__link}>
            <span className={styles.logo__text}>Pokemon App</span>
        </a>
    )
}