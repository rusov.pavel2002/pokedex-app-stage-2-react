import Logo from "../Logo/Logo"
import Nav from "../Nav/Nav"

import styles from './Header.scss'

export default function Header() {
    return (
        <header className={styles.header}>
            <div className={`${styles.container} ${styles.header__container}`}>
                <Logo />
                <Nav />
            </div>
        </header>
    )
}