import styles from './PokemonCard.scss'

import { PokemonData } from "@/model/model";

import { PokemonService } from '@/service/service';

const pokemonServiceInstance = PokemonService.getInstance();

export default function PokemonCard({avatar, name, id}: PokemonData) {
    return (
        <div className={styles.cards__item}>
            <img className={styles.card__image} src={avatar} alt={name} />
            <h3 className={styles.card__name}>{name.toUpperCase()}</h3>
            <h2 className={styles.card__id}>Pokemon ID: {id}</h2>
        </div>
    )
    // создаю способность покемона
    // const abilityHTML = document.createElement('span');
    // abilityHTML.classList.add(styles.card__ability);
    // abilityHTML.innerText = pokemonAbility;

    // создаю статус покемона
    // const statusHTML = document.createElement('span');
    // statusHTML.classList.add(styles.card__status);
    // statusHTML.innerText = pokemonStatus ? 'Пойман' : 'На свободе!';

    // если покемон пойман, то передаем дату поимки покемона
    // const catchedDateHTML = document.createElement('span');
    // catchedDateHTML.classList.add(styles.card__date);
    // catchedDateHTML.innerText = pokemonStatus ? pokemonCatchedTime.toDateString() : '';

    // создаю кнопку карточки
    // const catchedBtnHTML = document.createElement('button');
    // catchedBtnHTML.addEventListener('click', pokemonServiceInstance.clickedBtn);
    // catchedBtnHTML.classList.add(styles.btn);
    // catchedBtnHTML.setAttribute('data-id', pokemonId.toString());
    // catchedBtnHTML.innerText = 'Поймать!';

    // добавляю информацию о покемоне в карточку
}