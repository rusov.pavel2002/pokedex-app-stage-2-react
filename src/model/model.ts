// Add all interfaces into this directory

export interface PokemonData {
    id?: number,
    name: string,
    url?: string,
    avatar?: string,
    catched?: boolean
}

export interface PaginationInterface {
    countPokemons: number,
    currentPokemons: number
}