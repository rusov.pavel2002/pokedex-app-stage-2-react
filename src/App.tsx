import { Routes, Route } from 'react-router-dom'
import Home from '@/pages/Home'
// import Home from './pages/Home/Home'
// import Footer from './components/Footer/Footer'
// import './App.css'

export default function App() {
  return (
    <>
      <Routes>
        <Route path='/' element={<Home />}></Route>
      </Routes>
      {/* <Footer /> */}
    </>
  )
}


