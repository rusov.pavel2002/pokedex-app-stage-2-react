import Header from "@/view/Header/Header";
import Main from "@/view/Main/Main";

export default function Home() {
    return (
        <>
            <Header />
            <Main />
        </>
    )
}