// Add all objects with data into this directory
import { PokemonData, PaginationInterface } from "@/model/model";

export const pokemonStore: Array<PokemonData> = [];
export const paginationData: PaginationInterface = {
    countPokemons: 10000,
    currentPokemons: 0
};

